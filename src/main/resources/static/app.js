var stompClient = null;
var units = {};
var robotId = 0;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
}

function connect() {
    var socket = new SockJS('/robocode');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/robot', function (robotBot) {
            document.getElementById('gametime').innerHTML = new Date();
            console.log(robotBot);
            var robotBody = JSON.parse(robotBot.body);
            if(robotBody.name === 'red'){
                createUnitBotAndMove(robotId++, getRandomInt(getRandomInt(robotBody.x)), getRandomInt(getRandomInt(robotBody.y)), robotBody.name);
            }
            createUnitBotAndMove(robotId++, getRandomInt(getRandomInt(50)), getRandomInt(getRandomInt(50)), 'blue');
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function createYourRobot() {
    var optimus = {};
    optimus.id = robotId++;
    optimus.name = $("#name").val();
    optimus.state = true;
    optimus.health = 100;
    optimus.x = $("#positionx").val();
    optimus.y = $("#positiony").val();
    optimus.direction = "W";

    stompClient.send("/app/robot", {}, JSON.stringify(optimus));
    createYourUnitAndMove(robotId, $("#positionx").val(), $("#positiony").val(), 'red');
}

function createUnitBotAndMove(id, x, y, color) {
    console.log("x: " + x);
    console.log("y: " + y);

    var board = document.getElementById('board');
    let unit = {id, x, y, color};
    units[id] = unit;

    var cir1 = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    cir1.setAttribute("cx", x);
    cir1.setAttribute("cy", y);
    cir1.setAttribute("r", "2");
    cir1.setAttribute("fill", color);

    // attach it to the container
    board.appendChild(cir1);
    setTimeout(function(){ board.removeChild(cir1); }, 1800);
    unit.cir1 = cir1;
}

function createYourUnitAndMove(id, x, y, color) {
    console.log("x: " + x);
    console.log("y: " + y);

    var board = document.getElementById('board');
    let unit = {id, x, y, color};
    units[id] = unit;

    var cir1 = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    cir1.setAttribute("cx", x);
    cir1.setAttribute("cy", y);
    cir1.setAttribute("r", "2");
    cir1.setAttribute("fill", color);

    // attach it to the container
    board.appendChild(cir1);
    unit.cir1 = cir1;
}

function moveUnit(id, x, y) {
    let cir1 = units[id].cir1;
    cir1.setAttribute("cx", x);
    cir1.setAttribute("cy", y);
}

function getRandomInt(max) {
  var randomInt = Math.floor(Math.random() * Math.floor(max));
  if(randomInt >= 1  && randomInt <= 799){
    return randomInt;
  } else {
    return max;
  }

}

$(function () {
    $("form").on('submit', function (e) {e.preventDefault();});
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { createYourRobot(); });
});