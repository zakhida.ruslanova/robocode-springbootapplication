package se.mane.robocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobocodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RobocodeApplication.class, args);
    }

}
