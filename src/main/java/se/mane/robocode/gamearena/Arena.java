package se.mane.robocode.gamearena;

public class Arena {
    int weight;
    int height;

    public Arena(int weight, int height) {
        this.weight = weight;
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Arena{" +
                "weight=" + weight +
                ", height=" + height +
                '}';
    }
}
