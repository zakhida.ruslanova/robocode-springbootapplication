package se.mane.robocode.resource;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import se.mane.robocode.model.RobotResponse;
import java.util.ArrayList;
import java.util.List;


@Controller
public class RobotController {

    public static List<RobotResponse> robotResponseList = new ArrayList<>();

    @MessageMapping("/robot")
    @SendTo("/topic/robot")
    public RobotResponse getRobotResponse(RobotResponse robotResponse){
        System.out.println("optimus" + robotResponse);

        if(robotResponse == null) throw new IllegalArgumentException("robotResponse is null");
        robotResponseList.clear();
        robotResponseList.add(robotResponse);

        return new RobotResponse(
                robotResponse.getId(),
                robotResponse.getName(),
                robotResponse.isState(),
                robotResponse.getHealth(),
                robotResponse.getX(),
                robotResponse.getY(),
                robotResponse.getDirection());
    }
}