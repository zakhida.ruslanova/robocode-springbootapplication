package se.mane.robocode.robotsdetails;

public enum Direction {
    N, E, S, W
}
