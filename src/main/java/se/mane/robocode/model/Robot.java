package se.mane.robocode.model;

import se.mane.robocode.action.Action;
import se.mane.robocode.robotsdetails.Direction;
import se.mane.robocode.robotsdetails.Position;

abstract class Robot extends Action {
    private int id;
    private String name;
    private boolean state;
    private int health;
    private Position position;
    private Direction direction;

    public Robot(){}

    public Robot(int id, String name, boolean state, int health, Position position, Direction direction) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.health = health;
        this.position = position;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state=" + state +
                ", health=" + health +
                ", position=" + position +
                ", direction=" + direction +
                '}';
    }
}