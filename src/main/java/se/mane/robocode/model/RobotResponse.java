package se.mane.robocode.model;

public class RobotResponse {
    private int id;
    private String name;
    private boolean state;
    private int health;
    private double x;
    private double y;
    private String direction;

    public RobotResponse() {
    }

    public RobotResponse(int id, String name, boolean state, int health, double x, double y, String direction) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.health = health;
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "RobotResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", state=" + state +
                ", health=" + health +
                ", x=" + x +
                ", y=" + y +
                ", direction='" + direction + '\'' +
                '}';
    }
}
