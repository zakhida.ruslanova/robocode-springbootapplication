package se.mane.robocode.model;

import se.mane.robocode.robotsdetails.Direction;
import se.mane.robocode.robotsdetails.Position;

public class Optimus extends Robot{

    public Optimus(){
        super();
    }

    public Optimus(int id, String name, boolean state, int health, Position position, Direction direction) {
        super(id, name, state, health, position, direction);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public boolean isState() {
        return super.isState();
    }

    @Override
    public void setState(boolean state) {
        super.setState(state);
    }

    @Override
    public int getHealth() {
        return super.getHealth();
    }

    @Override
    public void setHealth(int health) {
        super.setHealth(health);
    }

    @Override
    public Position getPosition() {
        return super.getPosition();
    }

    @Override
    public void setPosition(Position position) {
        super.setPosition(position);
    }

    @Override
    public Direction getDirection() {
        return super.getDirection();
    }

    @Override
    public void setDirection(Direction direction) {
        super.setDirection(direction);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
