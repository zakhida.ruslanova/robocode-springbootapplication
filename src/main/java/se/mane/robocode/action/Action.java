package se.mane.robocode.action;

import se.mane.robocode.robotsdetails.Direction;
import se.mane.robocode.robotsdetails.Position;

public class Action implements RobotsActions {

    private static final int MAX_X = 800;
    private static final int MAX_Y = 800;

    @Override
    public void run(Position position, Direction direction) {
        System.out.println("Robots start position X: " + position.getX() + "position Y: " + position.getY());
    }


    @Override
    public boolean isMoving(Position position) {
        double positionX;
        double positionY;
        boolean isMoving = true;

        if ((position.getX() == MAX_X) || (position.getX() == 0) || (position.getY() == MAX_Y) || (position.getY() == 0)) {
            if (position.getX() == 0 || position.getY() == 0) {
                position.setX(position.getX() + 2);
                position.setY(position.getY() + 2);

                while ((position.getX() < MAX_X && position.getX() > 0) && (position.getY() < MAX_Y && position.getY() > 0)) {
                    System.out.println("Robots start position X: " + position.getX() + "position Y: " + position.getY());

                    positionX = position.getX() + 2;
                    positionY = position.getY() + 2;

                    position.setX(positionX);
                    position.setY(positionY);
                    isMoving = false;
                    System.out.println("Robots stop position X: " + position.getX() + "position Y: " + position.getY());
                }
            }
            if (position.getX() == MAX_X || position.getY() == MAX_Y) {
                position.setX(position.getX() - 2);
                position.setY(position.getY() - 2);

                while ((position.getX() < MAX_X && position.getX() > 0) && (position.getY() < MAX_Y && position.getY() > 0)) {
                    System.out.println("Robots start position X: " + position.getX() + "position Y: " + position.getY());

                    positionX = position.getX() - 2;
                    positionY = position.getY() - 2;

                    position.setX(positionX);
                    position.setY(positionY);
                    isMoving = false;
                    System.out.println("Robots stop position X: " + position.getX() + "position Y: " + position.getY());
                }
            }
        } else {
            if ((position.getX() == MAX_X) || (position.getX() == 0) || (position.getY() == MAX_Y) || (position.getY() == 0)) {
            }
            while ((position.getX() < MAX_X && position.getX() > 0) && (position.getY() < MAX_Y && position.getY() > 0)) {
                System.out.println("Robots start position X: " + position.getX() + "position Y: " + position.getY());

                positionX = position.getX() - 2;
                positionY = position.getY() - 2;

                position.setX(positionX);
                position.setY(positionY);
                isMoving = false;
                System.out.println("Robots stop position X: " + position.getX() + "position Y: " + position.getY());
            }
        }
        return isMoving;
    }

    @Override
    public void ahead(Direction direction) {

    }

    @Override
    public Direction getDefaultDirection() {
        return Direction.N;
    }

    @Override
    public Direction turnRight(Direction currentDirection) {
        switch (currentDirection) {
            case N:
                return Direction.E;
            case E:
                return Direction.S;
            case S:
                return Direction.W;
            default:
                return Direction.N;
        }
    }

    @Override
    public Direction turnLeft(Direction currentDirection) {
        switch (currentDirection) {
            case N:
                return Direction.W;
            case W:
                return Direction.S;
            case S:
                return Direction.E;
            default:
                return Direction.N;
        }
    }

    @Override
    public void shoot() {

    }

    @Override
    public void recreation() {

    }

    @Override
    public void searchEnemy() {

    }


    public static int generatRandomPositiveNegitiveValue(int max, int min) {
        int ii = -min + (int) (Math.random() * ((max - (-min)) + 1));
        return ii;
    }
}
