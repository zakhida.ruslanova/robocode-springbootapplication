package se.mane.robocode.action;

import se.mane.robocode.robotsdetails.Direction;
import se.mane.robocode.robotsdetails.Position;

public interface RobotsActions{
    void run(Position position,Direction direction);
    boolean isMoving(Position position);
    void ahead(Direction direction);
    Direction getDefaultDirection();
    Direction turnRight(Direction currentDirection);
    Direction turnLeft(Direction currentDirection);
    void shoot();
    void recreation();
    void searchEnemy();
}