package se.mane.robocode.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import se.mane.robocode.model.RobotResponse;

import static se.mane.robocode.resource.RobotController.robotResponseList;

@EnableScheduling
@Configuration
public class SchedulerConfig {

    private static final String DESTINATION = "/topic/robot";

    @Autowired
    SimpMessagingTemplate template;

    @Scheduled(fixedDelay = 2000)
    public void sendRobotBot(){
        template.convertAndSend(DESTINATION, new RobotResponse(1,"OptimusBot", true, 100, 250, 350, "W"));
        if(!robotResponseList.isEmpty()){
            template.convertAndSend(DESTINATION, robotResponseList.get(0));
        }
    }

}
