package se.mane.robocode.mapper;

import se.mane.robocode.model.Optimus;
import se.mane.robocode.model.RobotResponse;
import se.mane.robocode.robotsdetails.Position;

public class RobotMapper {

    public static Optimus mappRobotResponseToRobotEntity(RobotResponse robotResponse){
        Optimus optimus = new Optimus();
        optimus.setId(robotResponse.getId());
        optimus.setName(robotResponse.getName());
        optimus.setHealth(robotResponse.getHealth());
        optimus.setState(robotResponse.isState());
        optimus.setPosition(new Position(robotResponse.getX(), robotResponse.getY()));
        return optimus;
    }
}